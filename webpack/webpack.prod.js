const loaders = require("./loaders");
const preloaders = require("./preloaders");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: ['./src/index.ts'],
    output: {
        filename: 'build.js',
        path: 'public'
    },
    resolve: {
        root: __dirname,
        extensions: ['', '.ts', '.js', '.json']
    },
    resolveLoader: {
        modulesDirectories: ["node_modules"]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body',
            hash: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            warning: false,
            mangle: false,
            comments: false,
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.jquery': 'jquery'
        }),
        new CopyWebpackPlugin([{
            from: 'src/assets',
            to: '../public/assets'
        },
        {
            from: 'src/data',
            to: '../public'
        },
        ]),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('prod'),
                'API_URL': JSON.stringify('https://hospital-311.herokuapp.com/')
            }
        })
    ],
    module: {
        preLoaders: preloaders,
        loaders: loaders
    }
};

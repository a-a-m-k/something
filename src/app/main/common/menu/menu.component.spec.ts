import { } from 'jasmine';
import * as angular from 'angular';
import ngMaterial = require('angular-material');
import 'angular-mocks';
import './menu.module';

describe('Menu Component', () => {

  let scope;
  let componentController: any;
  let ctrl: any;
  let mdSidenavMock: any;

  beforeEach(angular.mock.module('MenuModule', ngMaterial));

  beforeEach(angular.mock.module(($provide) => {

    $provide.factory('$mdSidenav', function () {
      return function (sideNavId) {
        return { toggle: mdSidenavMock };
      };
    }); 

  }));

  beforeEach(angular.mock.inject(($rootScope, _$componentController_) => {

    scope = $rootScope.$new();
    componentController = _$componentController_;
    ctrl = componentController('menuComponent', { $scope: scope });

  }));

  it(`checks if ngOnInit hook define the right values of variables`, () => {

    const MENUITEMSDATAMock = [
      {
        name: 'Overview',
        icon: 'poll',
        url: 'overview'
      },
      {
        name: 'Patients',
        icon: 'people',
        url: 'patients'
      },
      {
        name: 'Admin',
        icon: 'account_circle',
        url: 'admin'
      }
    ];

    ctrl.ngOnInit();

    expect(ctrl.menuState).toBe('keyboard_arrow_right');
    expect(ctrl.menuItemsData).toEqual(MENUITEMSDATAMock);
  });

  it(`checks if 'toggleLeft' method calls 'buildToggler' and 'changeMenuState' methods`, () => {

    spyOn(ctrl, 'buildToggler');
    spyOn(ctrl, 'changeMenuState');

    ctrl.toggleLeft();

    expect(ctrl.buildToggler).toHaveBeenCalledTimes(1);
    expect(ctrl.changeMenuState).toHaveBeenCalledTimes(1);
  });

  it(`checks if 'buildToggler' and 'changeMenuState' methods are called with right parameters inside
of method 'toggleLeft'`, () => {

    ctrl.menuState = 'keyboard_arrow_left';

    spyOn(ctrl, 'buildToggler');
    spyOn(ctrl, 'changeMenuState');

    ctrl.toggleLeft();

    expect(ctrl.buildToggler).toHaveBeenCalledWith('sidebar');
    expect(ctrl.changeMenuState).toHaveBeenCalledWith('keyboard_arrow_left');
  });


  it(`checks if 'buildToggler' method calls 'toggle' method from Angular Material service '$mdSidenav'`, () => {
    
    const componentId = 'sidebar';
    
    mdSidenavMock = jasmine.createSpy('');

    ctrl.buildToggler(componentId);

    expect(mdSidenavMock).toHaveBeenCalledTimes(1);
  });

  it(`checks if 'changeMenuState' method changes menu states in a proper way`, () => {

    const state = 'keyboard_arrow_left';
    ctrl.menuState = 'keyboard_arrow_left';

    ctrl.changeMenuState(state);

    expect(ctrl.menuState).toBe('keyboard_arrow_right');
  });

  it(`checks if 'changeMenuState' method changes menu states in a proper way`, () => {

    const state = 'keyboard_arrow_right';
    ctrl.menuState = 'keyboard_arrow_right';

    ctrl.changeMenuState(state);

    expect(ctrl.menuState).toBe('keyboard_arrow_left');
  });

});
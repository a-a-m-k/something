import * as _ from 'lodash';
import './widget-table.component.scss';
import { WidgetTableService } from './widget-table.service';
import { CONSTANTS } from './widget-table.constant';
import { Component, Input, OnChanges } from 'angular-ts-decorators';

const templateUrl = require('./widget-table.component.html');

@Component({
    selector: 'widget-table',
    template: templateUrl
})
export class WidgetTableComponent implements OnChanges {
    @Input() widget;
    @Input() property;
    @Input() data;

    static $inject = ['WidgetTableService'];

    gridOptions: any;
    widgetName: string;
    private namebtn: string = CONSTANTS.previous;
    private index = 0;

    constructor(
        private WidgetTableService: WidgetTableService,
    ) { };

    ngOnChanges() {
        this.widgetName = _.upperFirst(this.widget);
        this.gridOptions = this.WidgetTableService.getGridOptions(this.widget, this.index, this.property);
        this.gridOptions.data = [this.data];
    }

    previousLatest() {
        this.namebtn = (this.namebtn === CONSTANTS.previous) ? CONSTANTS.latest : CONSTANTS.previous;
        this.index = (this.namebtn === CONSTANTS.latest) ? 1 : 0;
        this.gridOptions.columnDefs = this.WidgetTableService.getColumnDefs(this.widget, this.index, this.property);
    }
}

import * as angular from 'angular';
import { IScope, IAugmentedJQuery, IAttributes, INgModelController } from 'angular';
import { Directive } from 'angular-ts-decorators';

@Directive({
  selector: 'email-validation-directive',
  require: 'ngModel',
 restrict:'A',
 link:function(scope: IScope, element: IAugmentedJQuery, attrs: IAttributes, ngModel: INgModelController) {
    ngModel.$parsers.push(function(value) {
        ngModel.$setValidity('whitespaces', /^(\S+)$/.test(value)); 
        ngModel.$setValidity('email', /^[0-9a-zA-Z_]+([.][0-9a-zA-Z_]+)?@[0-9a-z_]+(\.[0-9a-z]{1,3})?(\.[a-z]{1,3})?(\.[a-z]{1,3})?\.[^0-9][a-z]{1,3}$/.test(value)); 
        return value;
      });
    
  }

})
export class EmailValidationDirective {}
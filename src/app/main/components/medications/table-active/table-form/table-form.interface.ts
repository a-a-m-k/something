export interface EditedMeds {
  startDate: string,
  status: string,
  doctor: string,
  stopDate: string,
  by: string,
  drugs: string
}
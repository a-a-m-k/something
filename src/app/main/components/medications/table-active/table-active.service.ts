import * as _ from 'lodash';
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class TableActiveService {
  gridApi;
 
  getGridOptions() {
    return { 
      enableCellEditOnFocus: false,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 110,
      enableSorting: true,
      enableColumnMenus: false,
      modifierKeysToMultiSelectCells: true,
      keyDownOverrides: [{ keyCode: 39, ctrlKey: true }],
      showGridFooter: true,
    
    onRegisterApi: ( gridApi ) => {
      this.gridApi = gridApi;
    },
      columnDefs: this.getColumnDefs()
    };
  }
  
  getColumnDefs() {
    return [
      { name: 'id', enableCellEdit: false },
      { name: 'date', enableCellEdit: false },
      { name: 'doctor', enableCellEdit: false},
      { name: 'drugs', enableCellEdit: false},
      { name: 'sig', enableCellEdit: false},
      { name: 'edit',
        cellTemplate:`<table-form-component on-save-meds="grid.appScope.saveMeds($event)"></table-form-component>`}
    ];
  }  
}

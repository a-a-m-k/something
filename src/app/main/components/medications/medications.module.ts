import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { MedicationsComponent } from './medications.component';
import { TableActiveModule } from './table-active/table-active.module';
import { TableHistoryModule } from './table-history/table-history.module';

@NgModule({
  declarations: [MedicationsComponent],
  imports: [TableActiveModule.name, TableHistoryModule.name]
})

export class MedicationsModule {
}


 
  

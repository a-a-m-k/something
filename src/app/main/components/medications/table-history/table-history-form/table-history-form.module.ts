import * as angular from 'angular';
import { NgModule } from 'angular-ts-decorators';
import { TableHistoryFormComponent } from './table-history-form.component';
import { TableFormHistoryService } from './table-history-form.service';
import ngMaterial = require('angular-material');


@NgModule({
  imports: [
    ngMaterial,  
  ],
  declarations: [TableHistoryFormComponent],
  providers: [
    { provide: 'TableFormHistoryService', useClass: TableFormHistoryService }
  ]
})

export class TableHistoryFormModule {}


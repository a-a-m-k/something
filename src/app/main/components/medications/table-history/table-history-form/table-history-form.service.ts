import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class  TableFormHistoryService {
  static $inject = ['$http'];
    
  constructor(private $http: ng.IHttpService) {}
  
  public getEditedMeds() {
    return {
      startDate: '',
      doctor: '',
      stopDate: '',
      by: '',
      drugs: ''
    };
  }
}


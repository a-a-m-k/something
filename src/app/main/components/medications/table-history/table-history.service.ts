import * as _ from 'lodash';
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class TableHistoryService {
  gridApi;

  getGridOptions() {
    return { 
      enableCellEditOnFocus: false,
      enableHorizontalScrollbar: 0,
      enableVerticalScrollbar: 0,
      rowHeight: 110,
      enableSorting: false,
      enableColumnMenus: false,
      modifierKeysToMultiSelectCells: true,
      keyDownOverrides: [{ keyCode: 39, ctrlKey: true }],
      showGridFooter: true,
      onRegisterApi: ( gridApi ) => {
        this.gridApi = gridApi;
      },
      columnDefs: this.getColumnDefs()
    };
  }
  
  getColumnDefs() {
    return [
      { name: 'id', enableCellEdit: false },
      { name: 'startDate', enableCellEdit: false },
      { name: 'doctor', enableCellEdit: false },
      { name: 'stopDate', enableCellEdit: false},
      { name: 'stopBy', displayName:'By', enableCellEdit: false},
      { name: 'drugs', enableCellEdit: false},
      { name: 'edit',
        cellTemplate:`<table-history-form-component 
                        on-save-meds="grid.appScope.saveMeds($event)">
                      </table-history-form-component>`}
    ];
  }  
}

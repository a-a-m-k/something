import { DiagnosesController } from './diagnoses.controller';

export const DiagnosesComponent: angular.IComponentOptions = {
  
  bindings: {
    patient: '<'
  },
  template: require('./diagnoses.component.html'),
  controller: DiagnosesController
};



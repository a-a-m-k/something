import { Inject } from 'angular-ts-decorators';
import { Component, Input, Output, OnInit } from 'angular-ts-decorators';
import { SearchService } from './patient-search.service';
import * as Rx from 'rxjs/Rx';

const templateUrl = require('./patient-search.component.html');

@Component({  
  selector: 'searchComponent',
  template: templateUrl
})

export class SearchComponent implements OnInit {  
  @Input() patients;
  @Output() onChangeFilter;
  static $inject = ['SearchService'];
  el = document.getElementById('SearchBox');
  keyups$;

  constructor(private searchService: SearchService) {
  }

  ngOnInit() {
    Rx.Observable.fromEvent(this.el, 'keyup')
      .map((event: any) => event.target.value)
      .map(value => this.searchService.filterPatient(this.patients, value))
      .subscribe(res => this.onChangeFilter({ res }));
  }
}


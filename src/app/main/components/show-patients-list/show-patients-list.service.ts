import * as _ from 'lodash';
import { Injectable } from 'angular-ts-decorators';

@Injectable()
export class ShowPatientsListService {

  sortedpatients;

  sortPatientsList(patients) {
    this.sortedpatients = _.sortBy(patients, 'firstName');
    return this.sortedpatients;
  }
}

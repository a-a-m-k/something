import { } from 'jasmine';
import * as angular from 'angular';
import 'angular-mocks';
import * as _ from 'lodash';
import './show-patients-list.module';

describe('Show Patients List Service', () => {

  let service;
  const mockPatients = [
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      sex: 'male',
    },
    {
      id: 2,
      firstName: 'Ann',
      lastName: 'Bureau',
      sex: 'female',
    },
    {
      id: 3,
      firstName: 'Linda',
      lastName: 'Kaufman',
      sex: 'female',
    }
  ];
  const mockSortedPatients = [
    {
      id: 2,
      firstName: 'Ann',
      lastName: 'Bureau',
      sex: 'female',
    },
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      sex: 'male',
    },
    {
      id: 3,
      firstName: 'Linda',
      lastName: 'Kaufman',
      sex: 'female',
    }
  ];

  beforeEach(angular.mock.module('ShowPatientsListModule'));

  beforeEach(angular.mock.inject(($injector) => {

    service = $injector.get('ShowPatientsListService');
  }));

  it('should return sorted patients', () => {

    service.sortPatientsList(mockPatients);

    expect(service.sortedpatients).toEqual(mockSortedPatients);
  });

});
import './header.component.scss';

import { HeaderController } from './header.controller';

export const headerComponent: angular.IComponentOptions = {
    
    template: require('./header.component.html'),
    controller: HeaderController
};

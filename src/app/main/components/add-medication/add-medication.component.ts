import * as angular from 'angular';
import * as _ from 'lodash'; 
import './add-medication.component.scss';
import { Component, Input, OnInit, Inject } from 'angular-ts-decorators';
import * as PatientActions from '../../../actions/patient.actions';
import { ActiveMedication } from './add-medication.interface';


@Component({
  selector: 'addMedicationComponent',
  template: require('./add-medication.component.html')
})
export class AddMedicationComponent implements OnInit {
  @Input() patient;

  static $inject = ['AddMedicationService', '$element', '$ngRedux'];
  drugs: string[];
  searchTerm: string;
  doctors: string[];
  isShown: boolean = false;
  newMedication: ActiveMedication;
  active: ActiveMedication;
  addMedicationForm;

  constructor(@Inject('AddMedicationService') private addMedicationService, 
  private $element: any, 
  private $ngRedux) {};

  ngOnInit() {
    this.addMedicationService.getMedication().then((response) => {
      this.drugs = response.data.drugs;
      this.searchTerm;
    });
    this.doctors = this.addMedicationService.getDoctors();
    this.newMedication = this.addMedicationService.newMedication();
  }
  
  onSubmit() {
    this.patient = angular.copy(this.patient);
    const id = _.isEmpty(this.patient.medication.active) ? 0 : _.first(this.patient.medication.active)['id'] + 1;
    this.newMedication = { ...this.newMedication, id };
    this.active = this.patient.medication.active.concat(this.newMedication).sort((x,y) => y.id - x.id);
    this.patient = {...this.patient, medication: {...this.patient.medication, active: this.active}};
    this.$ngRedux.dispatch(PatientActions.addPatientMedication(this.patient));
    this.isShown = false;
  }

  showForm() {
    this.addMedicationForm.$setPristine();
    this.addMedicationForm.$setUntouched();
    this.newMedication = this.addMedicationService.newMedication();
    this.isShown = true;
  }

  clearSearchTerm() {
    this.searchTerm = '';
  }
}

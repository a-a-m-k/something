
export interface ActiveMedication {
    id: Number,
    date: Date, 
    doctor: string,
    status: string,
    drugs: string, 
    sig: string
}
import { } from 'jasmine';
import * as angular from 'angular';
import 'angular-mocks';
import './patient-info.module';

describe('Patient Info Component', () => {

  let scope;
  let componentController: any;
  let ctrl: any;
  const malePatient = {
    firstName: 'John',
    lastName: 'Smith',
    sex: 'male'
  }
  const femalePatient = {
    firstName: 'Linda',
    lastName: 'Brown',
    sex: 'female'
  }
  
  beforeEach(angular.mock.module('PatientInfoModule'));

  beforeEach(angular.mock.inject(($rootScope, _$componentController_) => {
    scope = $rootScope.$new();
    componentController = _$componentController_;
    ctrl = componentController('patientInfoComponent', { $scope: scope });    
    ctrl.ngOnChanges({ patient: { currentValue: malePatient } });
  }));

  it('should return male color of icon', () => {
    expect(ctrl.getIconColor()).toBe('lightSkyBlue');
  });

  it('should return female color of icon', () => {
    ctrl.ngOnChanges({ patient: { currentValue: femalePatient } });
    expect(ctrl.getIconColor()).toBe('pink');
  });

  it('should return initials of patient', () => {
    expect(ctrl.getInitials()).toBe('JS');
  });

  it('should return full name of patient', () => {
    expect(ctrl.getFullName()).toBe('John Smith');
  }); 

  it('should set patient', () => {   
    expect(ctrl.patient).toEqual(malePatient);
  });

  it('should set full name', () => {
    expect(ctrl.fullName).toBe('John Smith');
  });

  it('should set initials', () => {
    expect(ctrl.initials).toBe('JS');
  });

  it('should set icon color', () => {    
    expect(ctrl.iconColor).toBe('lightSkyBlue');
  });

});





import { } from 'jasmine';
import * as angular from 'angular';
import 'angular-mocks';
import './patient-info.module';

describe('Patient Info Directive', () => {

  let scope;
  let element: any;
  let compiledElement: any;
  let compile: any;

  beforeEach(angular.mock.module('PatientInfoModule'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    compile = $compile;
    element = angular.element(
      '<div patient-icon-color color="pink"></div>'
    );
    compiledElement = compile(element)(scope);   
    scope.$digest(); 
  }));

  it('should set background-color', () => {
    expect(compiledElement.css('background-color')).toBe('pink');    
  }); 

});




